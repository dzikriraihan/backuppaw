@extends('layouts.main')

@section('main')
<div class="row">
  @include('partials.sideMenu')

  <div class="list-menu col">
    <div class="row">
        @foreach ((($title) === "Menu | Makanan" ? $makanan : $minuman) as $item)
          <div class="col-md-4">
            <div class="menu rounded-2" id="menu">
              <div class="row">
                <div class="foto col-md-3 rounded-circle"></div>
                <div class="makanan col-md-6">
                  <h6>{{ $item['name'] }}</h6>
                  <p>Rp {{ $item['price'] }}</p>
                </div>
                <!-- <div class="plus col-md-auto">
                  <a href="" class="btn btn-secondary rounded-circle">+</a>
                </div> -->
              </div>
            </div>
          </div>
        @endforeach
    </div>
  </div>
</div>
@endsection
