@extends('layouts.main')

@section('main')
<div class="row">
  @include('partials.sideMenu')

  <div class="list-menu col">
    <div class="row">

      @if($title === "Update Menu Makanan")
      <form method="POST" action="{{ route('food.update', $makanan->id) }}">
        @csrf
        @method('PUT')
        <div class="mb-3">
          <label for="namaMenu" class="form-label w-25">Nama Menu</label>
          <input type="input" class="form-control w-25" id="namaMenu" name="name" value="{{ $makanan['name'] }}">
        </div>
        <div class="mb-3">
          <label for="harga" class="form-label">Harga</label>
          <input type="number" class="form-control w-25" id="harga" name="price" value="{{ $makanan['price'] }}">
        </div>

        <div class="input-group mb-3 w-25">
          <label class="input-group-text" for="inputGroupSelect01">Options</label>
          <select class="form-select" id="inputGroupSelect01" name="option">
            <option selected>Pilih Jenis Menu</option>
            <option value="Minuman">Minuman</option>
            <option value="Makanan">Makanan</option>
          </select>
        </div>

        <div class="plus col-md-auto">
          <a href="{{ route('food.delete', $makanan->id) }}" class="btn" onclick="return confirm('Apakah anda yakin ingin menghapus menu?')"><span
              class="material-symbols-outlined">delete</span></a>
        </div>

        <button type="submit" class="btn btn-primary mt-3">Submit</button>
      </form>
</form>
      @elseif($title === "Update Menu Minuman")
      <form method="POST" action="{{ route('drinks.update', $minuman->id) }}">
        @csrf
        @method('PUT')
      <div class="mb-3">
        <label for="namaMenu" class="form-label w-25">Nama Menu</label>
        <input type="input" class="form-control w-25" id="namaMenu" name="name" value="{{ $minuman['name'] }}">
      </div>
      <div class="mb-3">
        <label for="harga" class="form-label">Harga</label>
        <input type="number" class="form-control w-25" id="harga" name="price" value="{{ $minuman['price'] }}">
      </div>

      <div class="input-group mb-3 w-25">
        <label class="input-group-text" for="inputGroupSelect01">Options</label>
        <select class="form-select" id="inputGroupSelect01" name="option">
          <option selected>Pilih Jenis Menu</option>
          <option value="Minuman">Minuman</option>
          <option value="Makanan">Makanan</option>
        </select>
      </div>

      <div class="plus col-md-auto">
        <a href="{{ route('drinks.delete', $minuman->id) }}" class="btn" onclick="return confirm('Apakah anda yakin ingin menghapus menu?')"><span
            class="material-symbols-outlined">delete</span></a>
      </div>

      <button type="submit" class="btn btn-primary mt-3">Submit</button>
      </form>

      @endif

    </div>
  </div>
</div>
@endsection