      <!-- row untuk bagi side menu dan list menu -->
      <div class="side-menu col-md-3">
        <!--col untuk side menu -->
        <div class="row">
          <!-- <div class="col-md-1 bar">
          </div> -->
          <h1>Menu</h1>
          <h5>
            <a href="/" class="{{ $title === 'Menu | Makanan' ? 'text-dark' : 'text-secondary' }}"
              ><span class="material-symbols-rounded"> restaurant </span>Makanan
            </a>
          </h5>
          <h5>
            <a href="/drinks"  class="{{ $title === 'Menu | Minuman' ? 'text-dark' : 'text-secondary' }}">
              <span class="material-symbols-rounded"> wine_bar </span>Minuman
            </a>
          </h5>
          <h5>
            <a href="/add" class="{{ $title === 'Menu | Tambah Menu' ? 'text-dark' : 'text-secondary' }}">
              <span class="material-symbols-rounded"> add </span>Tambah Menu
            </a>
          </h5>
          <h5>
            <a href="/update" class="{{ $title === 'Update Menu' || $title === 'Update Menu Makanan' || $title === 'Update Menu Minuman' ? 'text-dark' : 'text-secondary' }}">
              <span class="material-symbols-rounded"> autorenew </span>Update
              Menu
            </a>
          </h5>
        </div>
      </div>
