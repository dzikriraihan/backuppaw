<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Drinks;

class PostController extends Controller
{
    public function index() {
        return view('menu', [
            "title" => "Menu | Makanan",
            "makanan" => Post::all()
        ]);
    }

    public function drinks() {
        return view('menu', [
            "title" => "Menu | Minuman",
            "minuman" => Drinks::all()
        ]);
    }

    public function add() {
        return view('add', [
            "title" => "Menu | Tambah Menu",
        ]);
    }

    public function update() {
        return view('update', [
            "title" => "Update Menu",
            "makanan" => Post::all(),
            "minuman" => Drinks::all()
        ]);
    }

    public function updateFood() {
        return view('updateFood', [
            "title" => "Update Menu Makanan",
            "makanan" => Post::all()
        ]);
    }

    public function updateDrinks() {
        return view('updateDrinks', [
            "title" => "Update Menu Minuman",
            "minuman" => Drinks::all()
        ]);
    }

    public function store(Request $request) {
        if($request->input('option') === "Makanan") {
            Post::create($request->all());
            return redirect("/");
        } elseif ($request->input('option') === "Minuman") {
            Drinks::create($request->all());
            return redirect("/drinks");
        }
    }

    public function show($id) {
        $makanan = Post::find($id);
    
        return view('updateMenu', [
            "title" => "Update Menu Makanan",
            "makanan" => $makanan
        ]);
    }

    public function showDrinks($id) {
        $minuman = Drinks::find($id);

        return view('updateMenu', [
            "title" => "Update Menu Minuman",
            "minuman" => $minuman
        ]);
    }
    
    public function delete($id) {
        $makanan = Post::find($id);

        $makanan->delete();
        return redirect("/update/updateFood");
    }

    public function deleteDrinks($id) {
        $minuman = Drinks::find($id);

        $minuman->delete();
        return redirect("/update/updateDrinks");
    }

    public function updateMakanan(Request $request, $id) {
        $makanan = Post::where('id', $id)->first()->update($request->all());
        // $makanan = Post::find($request);
        // $makanan->update($request);

        return redirect("/update/updateFood");
    }
    public function updateMinuman(Request $request, $id) {
        $minuman = Drinks::where('id', $id)->first()->update($request->all());
        // $minuman = Drinks::find($id);
        // $minuman->update($request);

        return redirect('/update/updateDrinks');
    }
}
